class ComputerPlayer

  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  # return a winning move if possible; otherwise, move randomly
  def get_move

    # try to return a winning mark
    winnable_count = 0
    pos = nil

    # check columns
    (0...board.grid.length).each do |col|
      (0...board.grid.length).each do |row|
        if board.grid[row][col] == @mark
          winnable_count += 1
        elsif board.empty?([row, col])
          pos = [row, col]
        end
        if winnable_count == (board.grid.length - 1) && pos != nil
          return pos
        end
      end
      # reset values
      pos = nil
      winnable_count = 0
    end

    # check rows
    (0...board.grid.length).each do |row|
      (0...board.grid.length).each do |col|
        if board.grid[row][col] == @mark
          winnable_count += 1
        elsif board.empty?([row, col])
          pos = [row, col]
        end
        if winnable_count == (board.grid.length - 1) && pos != nil
          return pos
        end
      end
      # reset values
      pos = nil
      winnable_count = 0
    end

    # check left diagonal
    (0...board.grid.length).each do |el|
      if board.grid[el][el] == @mark
        winnable_count += 1
      elsif board.empty?([el, el])
        pos = [el, el]
      end
    end

    if winnable_count == (board.grid.length - 1) && pos != nil
      return pos
    end

    # reset values
    pos = nil
    winnable_count = 0

    # check right diagonal
    (0...board.grid.length).each do |el|
      if board.grid[el][board.grid.length - 1 - el] == @mark
        winnable_count += 1
      elsif board.empty?([el, board.grid.length - 1 - el])
        pos = [el, board.grid.length - 1 - el]
      end
    end

    if winnable_count == (board.grid.length - 1) && pos != nil
      return pos
    end
    # reset values
    pos = nil
    winnable_count = 0

    # if no winning, move, return random move
    pos = [rand(0...board.grid.length), rand(0...board.grid.length)]
  end

  # print board to screen
  def display(board)
    @board = board

    (0...board.grid.length).each do |col|
      (0...board.grid.length).each do |row|
        pos = [row, col]
        if board.empty?(pos)
          print "[ ]"
        elsif board[pos] == :X
          print "[X]"
        elsif board[pos] == :O
          print "[O]"
        end
      end
      print "\n"
    end
  end


end
