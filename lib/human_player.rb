class HumanPlayer
  attr_accessor :name, :mark, :board

  def initialize(name)
    @name = name
  end

  # ask for a move and return a position
  def get_move
    print "where"
    move = gets.chomp
    move = move.split(", ")

    # return position correctly formatted
    [move[0].to_i, move[1].to_i]
  end

  # print board to screen
  def display(board)
    (0...board.grid.length).each do |col|
      (0...board.grid.length).each do |row|
        pos = [row, col]
        if board.empty?(pos)
          print "[ ]"
        elsif board[pos] == :X
          print "[X]"
        elsif board[pos] == :O
          print "[O]"
        end
      end
      print "\n"
    end
  end

end
