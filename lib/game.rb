require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

# In your Game class, set the marks of the players you are passed.

class Game

  attr_accessor :board, :player_one, :player_two

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @player_one.mark = :X
    @player_two.mark = :O
    @current_player = player_one
    @board = Board.new
  end

  def current_player
    @current_player
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

  # handles the logic for a single turn
  def play_turn
    @board.place_mark(@current_player.get_move, @current_player.mark)
    @current_player.display(board)
    switch_players!
  end

  # calls play_turn each time through a loop until the game is over
  def play
    until board.over?
      play_turn
    end
    puts "Game is over!"
  end

end
