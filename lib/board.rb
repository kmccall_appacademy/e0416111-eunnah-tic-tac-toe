class Board
  attr_accessor :grid

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    # throw error if the pos isn't empty
    if !empty?(pos)
      raise "Square not empty!"
    else
      self[pos] = mark
    end

  end

  def empty?(pos)
    if self[pos] == nil
      true
    else
      false
    end
  end

  def winner
    # should return a mark
    # check columns
    (0...grid.length).each do |col|
      if (0...grid.length).all? { |row| @grid[row][col] == :X }
        return :X
      elsif (0...grid.length).all? { |row| @grid[row][col] == :O }
        return :O
      end
    end

    # check rows
    (0...grid.length).each do |row|
      if (0...grid.length).all? { |col| @grid[row][col] == :X }
        return :X
      elsif (0...grid.length).all? { |col| @grid[row][col] == :O }
        return :O
      end
    end

    # check left diagonal
    if (0...grid.length).all? { |el| @grid[el][el] == :X }
      return :X
    elsif (0...grid.length).all? { |el| @grid[el][el] == :O }
      return :O
    end

    # check right diagonal
    if (0...grid.length).all? { |el| @grid[el][grid.length - 1 - el] == :X }
      return :X
    elsif (0...grid.length).all? { |el| @grid[el][grid.length - 1 - el] == :O }
      return :O
    end

    # if no winner, return nil
    nil
  end

  def over?

    # over when the game is won
    if winner
      return true
    end

    # is not over when at least one square on the board is empty
    (0...grid.length).each do |col|
      if (0...grid.length).any? { |row| empty?([row, col]) }
        return false
      end
    end

    # is over since all squares are full, meaning there is a tie
    true

  end


end
